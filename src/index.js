import 'bootstrap/dist/css/bootstrap.min.css'
import App from './components/App.jsx'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import { addPosts } from './reducer/posts.js'
import createLogger from 'redux-logger'
import { getPosts } from './api/getPosts.js'
import { getUsers } from './api/getUsers.js'
import { reducer } from './reducer.js'
import { applyMiddleware, createStore } from 'redux'

const fullPosts = Promise.all([ getPosts(), getUsers() ])
.then(([ posts, users ]) =>
  posts.map(post => ({
    key: post.id,
    userId: post.userId,
    name: users[post.userId],
    text: post.body,
    id: post.id
  }))
)

const store = createStore(reducer, applyMiddleware(createLogger()))
// const filter = store.filter
// const posts = store.posts

const dispatch = store.dispatch


fullPosts.then(posts => {
  store.dispatch(addPosts(posts))
})


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('yo')
)

