import React from 'react'
import { addPost } from '../reducer/posts.js'
import { connect } from 'react-redux'

export class AddPost extends React.Component {
  render() {
    return (
      <div>
        <input
          className="col-md-3"
          placeholder="Имя"
          type="text"
          ref={element => {
            this.name = element
          }}
        />
        <input
          className="col-md-offset-1 col-md-5 row"
          placeholder="Текст"
          type="text"
          ref={element => {
            this.text = element
          }}
        />
        <input
          className="col-md-offset-1 col-md-1"
          value="Отправить"
          type="submit"
          onClick={
            () => {
              this.props.addPost(this.name.value, this.text.value)
              this.name.value = ''
              this.text.value = ''
            }
          }
        />
      </div>
    )
  }
}

function dispatchToProps(dispatch) {
  return {
    addPost(name, text) { // eslint-disable-line no-shadow
      if (name.trim() && text.trim()) {
        dispatch(
          addPost(name, text)
        )
      }
    }
  }
}

export default connect(null, dispatchToProps)(AddPost)
