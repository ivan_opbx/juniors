import AddPost from './AddPost.jsx'
import FilterForm from './FilterForm.jsx'
import Posts from './Posts.jsx'
import React from 'react'

export class App extends React.Component {
  render() {
    return (
      <div className="container">
        <br />
        <AddPost />
        <br />
        <br />
        <FilterForm />
        <br />
        <Posts />
      </div>
    )
  }
}
App.displayName = 'App'

export default App
