export default function filter(oldPosts = [], action) {
  switch (action.type) {
    case 'ADD_POSTS':
      return oldPosts.concat(action.payload)
    default:
      return oldPosts
  }
}

export function getFiltered(posts, filtered) {
  if (!filtered || filtered <= 0) {
    return posts
  }
  return posts.filter(({ userId }) => userId == filtered)
}

export function addPosts(addPosted) {
  return {
    type: 'ADD_POSTS',
    payload: addPosted
  }
}

export function addPost(name, text) {
  return {
    type: 'ADD_POSTS',
    payload: [ { userId: 11, id: Math.floor(Math.random() * 100000) + 100, name, text } ]
  }
}
