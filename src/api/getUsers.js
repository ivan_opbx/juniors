export function getUsers() {
  const users = fetch('//dev.onlinepbx.ru/users.php')
    .then(response => response.json())
    .then(response => response.reduce(
    (usersed, user) => Object.assign({}, usersed, {
      [user.id]: user.name
    }),
  {}
  ))
  return users
}
